<?php 

 return [
    "password" => "Passwords must be at least six characters and match the confirmation.",
    "token" => "Il token per resettare la password non è valido",
    "user" => "We can't find a user with that e-mail address.",
    "sent" => "We have e-mailed your password reset link!",
    "reset" => "Your password has been reset!"
];