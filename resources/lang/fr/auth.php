<?php 

 return [
    "failed" => "Queste credenziali non coincidono con i nostri record",
    "throttle" => "Troppi login . Riprova più tra :seconds secondi"
];